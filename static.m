% Solid model
% -----------
% Compute static response of plate meshed with quadratic 3D solid hexaedra

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Plate size
at = 1;
bt = 1;
ct = 0.1;

% Material and section data
E = 2e11;
nu = 0.3;

% FE model
% --------

Na = 10;
Nb = 10;
Nc = 3;

dof = 3;
N = dof*(2*Na-1)*(2*Nb-1)*(2*Nc-1); % size of the problem

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 
c = ct/(Nc-1);

% Create stiffness matrix for each layer

% Integration points
% intP = 2; % reduced integration
intP = 3; % full integration

[K_N, ~] = elementary(a,b,c,nu);
ke = E*a/2*b/2*c/2*gaussIntegrate(K_N, intP);

% WARNING: three integration points needed to avoid hourglassing except for
% clamped case

% Connectivity
% ------------
connect = connect(Na, Nb, Nc);

% Assemble
% --------

K = assemble(ke, connect, N);

% Plot model
% ----------
connectOut = connect(:, [1,3,5,7,19,21,23,25]); % take only outer nodes

% Reduce numbering as if it had only outer nodes
% Map sorted numbers from connectOut to their pos in sorted vector

[nodes, order] = sort(unique(connectOut(:)));

for k = 1: Na*Nb*Nc
    connectOut(nodes(k)==connectOut) = k;
end

plotModel(Na, Nb, Nc, at, bt, ct, connectOut,'b')

% Boundary conditions
% -------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 2;

% Boundaries

low = repmat(1:2*Na-1,1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Na-1,1);
s = reshape(s, 1, numel(s));
low = low + s;

up = low + (2*Na-1)*(2*Nb-2); 

left  = repmat(1:(2*Na-1):(2*Nb-1)*(2*Na-1),1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Nb-1,1);
s = reshape(s, 1, numel(s));
left = left + s;

right = left + 2*Na-2; 

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1 
    % Simply supported in all edges. 
    % In x = 0 and y = 0 all DOFs are blocked, in x = L and y = L
    % displacement in direction of the beam is allowed
    
    low = reshape(low, numel(low)/(2*Nc-1), 2*Nc-1);    
    low = low(:,ceil(Nc/2))';
    
    up = reshape(up, numel(up)/(2*Nc-1), 2*Nc-1);    
    up = up(:,ceil(Nc/2))';
    
    right = reshape(right, numel(right)/(2*Nc-1), 2*Nc-1);    
    right = right(:,ceil(Nc/2))';
    
    left = reshape(left, numel(left)/(2*Nc-1), 2*Nc-1);    
    left = left(:,ceil(Nc/2))';
    
    border0 = [low left];
    borderL = [up right];
    % border = unique(border);
    
    node0 = reshape(repmat(border0,dof,1), [length(border0)*dof 1]);
    dofs0 = reshape(repmat(1:dof,1,length(border0)), [length(border0)*dof 1]);
    value0 = zeros(length(border0)*dof,1);
    
    nodeL = reshape(repmat(borderL,2,1), [length(borderL)*2 1]);
    dofsL = reshape(repmat([1,3],1,length(borderL)), [length(borderL)*2 1]);
    valueL = zeros(length(borderL)*2,1);
    
    node = [node0; nodeL];
    dofs = [dofs0; dofsL];
    value = [value0; valueL];
    
    BC = [node dofs value]; 
    
    % WARNING: corners belong to two borders

      
    case 2
    % Clamped in all edges
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clampled in one edge    
    border = low;
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
    case 5
    % Simply supported in all corners
    
    % Only take the middle point of the vertical edge
    
    lowLeft = lowLeft(ceil(Nc/2));
    lowRight = lowRight(ceil(Nc/2));
    upRight = upRight(ceil(Nc/2));
    upLeft = upLeft(ceil(Nc/2));
    
    border = [lowLeft lowRight upRight upLeft];
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
end

K_BC = applyBC(K, BC);

% Unit pressure
% -------------

% Pressure in lower face z = -1
startNode = 1:(2*(2*Na-1)):(2*(Nb-1)*(2*Na-1)); % first node of every row
% Repeat nodes Na-1 times
startNode = repmat(startNode, Na-1, 1);
% Add 2 to each rep
s = repmat(0:2:2*(Nb-2), 1, Na-1); 
% Add and sort
startNode = sort(startNode(:) + s(:)); % first node of every element

load = [zeros(size(startNode)) zeros(size(startNode)) ones(size(startNode))]; % pressure as [Px Py Pz]
face = [zeros(size(startNode)) zeros(size(startNode)) -1*ones(size(startNode))]; % [x y z]  

values = {startNode,load,face};

F = assembleForce(a, b, c, values, connect, N); % force applied on top layer
F_BC = applyBC(F,BC);

uStatic = K_BC\F_BC;

% Assemble static response
uStatic = assembleBC(uStatic, BC);

% Plot result
% -----------

% TODO

% RMS response of all points (RMS of modulus)
uStatic = uStatic(3:3:end,:); % each column is the response at a certain f
rms = mean(abs(uStatic).^2).^0.5;

% RMS: 3.3319e-11
% MAX: 8.0074e-11