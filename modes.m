% Computation of mode shapes of plate using FE
% --------------------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

Na = 8;    % number of nodes in x direction (outer nodes of element only)
Nb = 8;    % number of nodes in y direction (outer nodes of element only)
Nc = 3;    % number of nodes in z direction (outer nodes of element only)
dof = 3;   % number of dof/node

N = dof*(2*Na-1)*(2*Nb-1)*(2*Nc-1); % size of the problem

% Material and section data

% Plate size
at = 1;
bt = 1;
ct = 0.1;

% Material and section data
E = 2e11;
nu = 0.3;
rho = 7900;

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 
c = ct/(Nc-1);

% TODO: extract flags as plotting, number of integration points, number of
% modes to compute, boundary conditions


% Interpolation functions
% -----------------------

[K_N, M_N] = elementary(a,b,c,nu);


% Gaussian quadrature
% -------------------

% a/2, b/2 and c/2 to adjust interval from [-1 1; -1 1; -1 1] to 
% [0 a, 0 b, 0 c] 

% Integration points
% intP = 2; % reduced integration
intP = 3; % full integration

ke = E*a/2*b/2*c/2*gaussIntegrate(K_N, intP);
me = rho*a/2*b/2*c/2*gaussIntegrate(M_N, intP);

% Connectivity 
% -------------

Ne = (Na-1)*(Nb-1)*(Nc-1); % number of elements
connect = connect(Na, Nb, Nc);

% Plot model
connectOut = connect(:, [1,3,5,7,19,21,23,25]); % take only outer nodes

% Reduce numbering as if it had only outer nodes
% Map sorted numbers from connectOut to their pos in sorted vector

[nodes, order] = sort(unique(connectOut(:)));

for k = 1: Na*Nb*Nc
    connectOut(nodes(k)==connectOut) = k;
end

plotModel(Na, Nb, Nc, at, bt, ct, connectOut,'b')

K = assemble(ke, connect, N);
M = assemble(me, connect, N);


% Boundary conditions
% -------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 1;

% Boundaries
low = repmat(1:2*Na-1,1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Na-1,1);
s = reshape(s, 1, numel(s));
low = low + s;

up = low + (2*Na-1)*(2*Nb-2); 

left  = repmat(1:(2*Na-1):(2*Nb-1)*(2*Na-1),1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Nb-1,1);
s = reshape(s, 1, numel(s));
left = left + s;

right = left + 2*Na-2; 

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1 
    % Simply supported in all edges. 
    % In x = 0 and y = 0 all DOFs are blocked, in x = L and y = L
    % displacement in direction of the beam is allowed
    
    low = reshape(low, numel(low)/(2*Nc-1), 2*Nc-1);    
    low = low(:,ceil(Nc/2))';
    
    up = reshape(up, numel(up)/(2*Nc-1), 2*Nc-1);    
    up = up(:,ceil(Nc/2))';
    
    right = reshape(right, numel(right)/(2*Nc-1), 2*Nc-1);    
    right = right(:,ceil(Nc/2))';
    
    left = reshape(left, numel(left)/(2*Nc-1), 2*Nc-1);    
    left = left(:,ceil(Nc/2))';
    
    border0 = [low left];
    borderL = [up right];
    % border = unique(border);
    
    node0 = reshape(repmat(border0,dof,1), [length(border0)*dof 1]);
    dofs0 = reshape(repmat(1:dof,1,length(border0)), [length(border0)*dof 1]);
    value0 = zeros(length(border0)*dof,1);
    
    nodeL = reshape(repmat(borderL,2,1), [length(borderL)*2 1]);
    dofsL = reshape(repmat([1,3],1,length(borderL)), [length(borderL)*2 1]);
    valueL = zeros(length(borderL)*2,1);
    
    node = [node0; nodeL];
    dofs = [dofs0; dofsL];
    value = [value0; valueL];
    
    BC = [node dofs value]; 
    
    % WARNING: corners belong to two borders

    
    case 2
    % Clamped in all edges
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clampled in one edge    
    border = low;
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
    case 5
    % Simply supported in all corners
    
    % Only take the middle point of the vertical edge
    
    lowLeft = lowLeft(ceil(Nc/2));
    lowRight = lowRight(ceil(Nc/2));
    upRight = upRight(ceil(Nc/2));
    upLeft = upLeft(ceil(Nc/2));
    
    border = [lowLeft lowRight upRight upLeft];
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
end

K_BC = applyBC(K, BC);
M_BC = applyBC(M, BC);

% Eigenpairs
% ----------

modeN = 15;

[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');

omega = sqrt(diag(lambda));

fprintf('Natural frequencies [rad/s]\n')
fprintf('%.2f\n',omega)

% Apply BCs to modes

u = assembleBC(phi, BC);

% Plot results
% ------------

modeNum = 8;

mode = u(:, modeNum);
scale = 2;

result = scale*[mode(1:3:end) mode(2:3:end) mode(3:3:end)];
% Take apart displacement in x, y and z from the mode

plotResult(result, Na, Nb, Nc, at, bt, ct, connect)