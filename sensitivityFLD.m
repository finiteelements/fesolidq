% Solid model with layers of two materials
% ----------------------------------------
% Compute eigenpairs of a FLD plate meshed with quadratic 3D solid hexaedra

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Plate size
at = 0.1;
bt = 0.1;
ct1 = 0.002; % first layer 
ct2 = 0.002; % second layer 
% ct2 = 0.006;
% ct2 = 0.010;

% Material and section data
E1 = 1.762e11;
nu1 = 0.3;
rho1 = 7782;
nu2 = 0.3;
rho2 = 1423;

E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

% No fractional damping
E2 = E_0;

% Initialisation
modeN = 4;
omegaOld = zeros(modeN, 1);
tol = 20;
change = tol*2;

% FE model
% --------

% Layer 1
Na = 18;
Nb = 18;
Nc1 = 3;

% Layer 2
Nc2 = 3;

dof = 3;

while change > tol

N = dof*(2*Na-1)*(2*Nb-1)*(2*(Nc1+Nc2-1)-1); % size of the problem
N1 = dof*(2*Na-1)*(2*Nb-1)*(2*Nc1-1); % number of dof in first layer
N2 = dof*(2*Na-1)*(2*Nb-1)*(2*Nc2-1); % number of dof in second layer

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 
c1 = ct1/(Nc1-1);
c2 = ct2/(Nc2-1);

% Find interface
upperFace = (1:(2*Na-1)*(2*Nb-1))+(2*Na-1)*(2*Nb-1)*(2*Nc1-2);

% Create stiffness matrix for each layer

% Integration points
% intP = 2; % reduced integration
intP = 3; % full integration

[K_N1, M_N1] = elementary(a,b,c1,nu1);
ke1 = E1*a/2*b/2*c1/2*gaussIntegrate(K_N1, intP);
me1 = rho1*a/2*b/2*c1/2*gaussIntegrate(M_N1, intP);

[K_N2, M_N2] = elementary(a,b,c2,nu2);
ke2 = E2*a/2*b/2*c2/2*gaussIntegrate(K_N2, intP);
me2 = rho2*a/2*b/2*c2/2*gaussIntegrate(M_N2, intP);
% WARNING: three integration points needed to avoid hourglassing except for
% clamped case

% Connectivity
% ------------
% First layer
connect1 = connect(Na, Nb, Nc1);

K1 = assemble(ke1, connect1, N1);
M1 = assemble(me1, connect1, N1);

% Second layer
connect2 = connect(Na, Nb, Nc2);

K2 = assemble(ke2, connect2, N2);
M2 = assemble(me2, connect2, N2);

% Assemble

% Find indices and values of matrix
[i1,j1,v1] = find(K1);
[im1,jm1,vm1] = find(M1);

[i2,j2,v2] = find(K2);
[im2,jm2,vm2] = find(M2);

% Create complete matrix
K1 = sparse(i1, j1, v1, N, N);
M1 = sparse(im1, jm1, vm1, N, N);

% Shift and add matrix of second layer
X = dof*((2*Na-1)*(2*Nb-1)*(2*Nc1-1) - numel(upperFace));
K2 = sparse(i2+X, j2+X, v2, N, N);
M2 = sparse(im2+X, jm2+X, vm2, N, N);

K = K1 + K2;
M = M1 + M2;

% Boundary conditions
% -------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 4;

% Boundaries
Nc = Nc1+Nc2-1;

low = repmat(1:2*Na-1,1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Na-1,1);
s = reshape(s, 1, numel(s));
low = low + s;

up = low + (2*Na-1)*(2*Nb-2); 

left  = repmat(1:(2*Na-1):(2*Nb-1)*(2*Na-1),1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Nb-1,1);
s = reshape(s, 1, numel(s));
left = left + s;

right = left + 2*Na-2; 

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1 
    % Simply supported in all edges. 
    % In x = 0 and y = 0 all DOFs are blocked, in x = L and y = L
    % displacement in direction of the beam is allowed
    
    low = reshape(low, numel(low)/(2*Nc-1), 2*Nc-1);    
    low = low(:,ceil(Nc/2))';
    
    up = reshape(up, numel(up)/(2*Nc-1), 2*Nc-1);    
    up = up(:,ceil(Nc/2))';
    
    right = reshape(right, numel(right)/(2*Nc-1), 2*Nc-1);    
    right = right(:,ceil(Nc/2))';
    
    left = reshape(left, numel(left)/(2*Nc-1), 2*Nc-1);    
    left = left(:,ceil(Nc/2))';
    
    border0 = [low left];
    borderL = [up right];
    % border = unique(border);
    
    node0 = reshape(repmat(border0,dof,1), [length(border0)*dof 1]);
    dofs0 = reshape(repmat(1:dof,1,length(border0)), [length(border0)*dof 1]);
    value0 = zeros(length(border0)*dof,1);
    
    nodeL = reshape(repmat(borderL,2,1), [length(borderL)*2 1]);
    dofsL = reshape(repmat([1,3],1,length(borderL)), [length(borderL)*2 1]);
    valueL = zeros(length(borderL)*2,1);
    
    node = [node0; nodeL];
    dofs = [dofs0; dofsL];
    value = [value0; valueL];
    
    BC = [node dofs value]; 
    
    % WARNING: corners belong to two borders

    
    case 2
    % Clamped in all edges
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clampled in one edge    
    border = low;
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
    case 5
    % Simply supported in all corners
    
    % Only take the middle point of the vertical edge
    
    lowLeft = lowLeft(ceil(Nc/2));
    lowRight = lowRight(ceil(Nc/2));
    upRight = upRight(ceil(Nc/2));
    upLeft = upLeft(ceil(Nc/2));
    
    border = [lowLeft lowRight upRight upLeft];
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
end

K_BC = applyBC(K, BC);
M_BC = applyBC(M, BC);

% Separate into two to apply frequency dependent modulus
K1_BC = applyBC(K1, BC);
K2_BC = applyBC(K2, BC);

% Eigenvalues 
% -----------

[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');

omega = sqrt(diag(lambda));

change = max(abs(omega - omegaOld));

omegaOld = omega;
Na = Na + 1;
Nb = Nb + 1;

end

% Number of nodes needed
fprintf('Na = %d\nNb = %d\n', Na-1, Nb-1)

fprintf('Natural frequencies [rad/s]\n')
fprintf('%.2f\n',omegaOld)

% For simply supported Na = Nb = 26 needed for simply supported