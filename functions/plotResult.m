function plotResult(result, Na, Nb, Nc, at, bt, ct, connect)
% Plot deformed plate meshed with 3D hexaedra
%
% PLOTRESULT(result, Na, Nb, Nc, at, bt, ct, connect)
%
% result: matrix whose columns are displacements in x, y and z
% Na: number of nodes in x direction
% Nb: number of nodes in y direction
% Nc: number of nodes in z direction
% at: total length in x direction
% bt: total length in y direction
% ct: total length in z direction
% connect: connectivity of the elements
%
% See also PLOTMODEL

% Number of elements
Ne = (Na-1)*(Nb-1)*(Nc-1);

% Coordinates of nodes
x = linspace(0,at,2*Na-1); 
y = linspace(0,bt,2*Nb-1); 
z = linspace(0,ct,2*Nc-1); 

[X,Y,Z] = meshgrid(x,y,z);

% Coordinates of all points in the grid
x = reshape(permute(X,[2 1 3]),[numel(X), 1]);
y = reshape(permute(Y,[2 1 3]),[numel(Y), 1]);
z = reshape(permute(Z,[2 1 3]),[numel(Z), 1]);

% Add displacement value to coordinate
x = x + result(:,1);
y = y + result(:,2);
z = z + result(:,3);

coord = [x y z];

% Initialise
reorganised = zeros(size(coord));

n = get(gcf,'Number');
figure(n+1)

% Number of nodes in the element
nodeNum = 27;

for k = 1:Ne
    % Reorganise coord by element coordinates
    reorganised(nodeNum*(k-1)+1:nodeNum*(k-1)+nodeNum,:) = coord(connect(k,:),:);
end

cubePlotQ(reorganised, 'b', 0.8)

end