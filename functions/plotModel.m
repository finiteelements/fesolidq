function plotModel(Na, Nb, Nc, at, bt, ct, connect, colour)
% Plot plate model meshed with 3D hexaedra
%
% PLOTMODEL(Na, Nb, Nc, at, bt, ct, connect, colour)
%
% Na: number of nodes in x direction
% Nb: number of nodes in y direction
% Nc: number of nodes in z direction
% at: total length in x direction
% bt: total length in y direction
% ct: total length in z direction
% connect: connectivity of the elements
% colour: color of model

% Length of elements
a = at/(Na-1);
b = bt/(Nb-1);
c = ct/(Nc-1);

% Number of elements
Ne = (Na-1)*(Nb-1)*(Nc-1);

% Coordinates of nodes
x = 0:a:at; 
y = 0:b:bt;
z = 0:c:ct;

[X,Y,Z] = meshgrid(x,y,z);

% Coordinates of all points in the grid, permute is used to change the
% second and first dimensions.
x = reshape(permute(X,[2 1 3]),[numel(X), 1]);
y = reshape(permute(Y,[2 1 3]),[numel(Y), 1]);
z = reshape(permute(Z,[2 1 3]),[numel(Z), 1]);

coord = [x y z];

% Initialise
reorganised = zeros(size(coord));

figure(1)

for k = 1:Ne
    
    % Reorganise coord by element coordinates
    reorganised(8*(k-1)+1:8*(k-1)+8,:) = coord(connect(k,:),:);

end

cubePlot(reorganised, colour, 0.6)

% TODO: Label the points taking into account that are quadratic
% figure(1)
% offset = a/8;
% label = cellfun(@num2str,num2cell(1:Na*Nb*Nc),'UniformOutput',0);
% h = text(x+offset,y+offset,z+offset, label);
% set(h,'Color','w')

end