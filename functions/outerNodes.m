function outer = outerNodes(connect, Na, Nb, Nc)
% Given the connectivity of a model based on quadratic hexaedra and the
% number of nodes in each direction, extract the connectivity of the outer
% nodes
%
% outer = OUTERNODES(connectivity, Na, Nb, Nc)

outer = connect(:, [1,3,5,7,19,21,23,25]); % take only outer nodes

% Reduce numbering as if it had only outer nodes
% Map sorted numbers from connectOut to their pos in sorted vector

[nodes, ~] = sort(unique(outer(:)));

for k = 1: Na*Nb*Nc
    outer(nodes(k)==outer) = k;
end

end