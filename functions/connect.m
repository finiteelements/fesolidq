function connectivity = connect(Na, Nb, Nc)
% Creates connectivity matrix for plate given the number of nodes used to
% modelise it.
%
% connectivity = CONNECT(Na, Nb, Nc)
% 
% Na, Nb, Nc: number of outer nodes in the model in x, y and z directions

nodes = 1:(2*(2*Na-1)):(2*(Nb-1)*(2*Na-1)); % first node of every row

% Repeat nodes Na-1 times
nodes = repmat(nodes, Na-1, 1);
% Add 2 to each rep
s = repmat(0:2:2*(Na-1)-1, 1, Nb-1); 
% Add and sort
nodes = sort(nodes(:) + s(:)); % first node of every element

nodes = repmat(nodes,Nc-1,1); % repeat for each layer
s = repmat(2*(0:(2*Na-1)*(2*Nb-1):(Nc-2)*(2*Na-1)*(2*Nb-1))', 1, (Na-1)*(Nb-1));
s = reshape(s', numel(s),1); % add 2*number of nodes in each layer to numbers in layer 1

nodes = nodes + s;

connectivity = [nodes nodes+1 nodes+2 nodes+2*Na+1 nodes+4*Na nodes+4*Na-1 ...
           nodes+2*(2*Na-1) nodes+2*Na-1 nodes+2*Na];
connectivity = [connectivity connectivity+(2*Na-1)*(2*Nb-1) connectivity+2*(2*Na-1)*(2*Nb-1)];

end