function [K_N, M_N] = elementary(a,b,c,nu)
% Create elementary matrices
%
% [K_N, M_N] = ELEMENTARY(a,b,c,nu) 
% 
% a, b, c: lenghts of elements
% nu: Poisson's modulus
%
% See also INTERPOLATE

% For C3D27 interpolation functions x^i�y^j�z^k / {i,j,k} in {0,1,2} 
syms x y z
terms = x.^(0:2).'*y.^(0:2);
terms = terms(:)*z.^(0:2);
terms = terms(:).';
nodes = [-1 -1 -1; 0 -1 -1; 1 -1 -1; 1 0 -1; 1 1 -1; 0 1 -1; -1 1 -1; -1 0 -1; 0 0 -1];

nodes = [nodes; [nodes(:,1:2) nodes(:,3)+1]; [nodes(:,1:2) nodes(:,3)+2]]';

Ni = interpolate(terms, nodes);

dof = 3;
nodes = length(Ni);

% Initialise
N = sym(zeros(dof, dof*nodes));
B = sym(zeros(dof*2, dof*nodes));

for n = 1:nodes
    
    syms xi eta zeta
    
    % Matrix of interpolation functions
    % [Ni 0  0
    %  0  Ni 0
    %  0  0  Ni]
    
    N(:, 3*n-2:3*n) = Ni(n)*eye(dof);

    Nx = diff(Ni(n),xi);
    Ny = diff(Ni(n),eta);
    Nz = diff(Ni(n),zeta);
    
    % For chain rule
    xi  = 2*x/a - 1; 
    eta = 2*y/b - 1;
    zeta = 2*z/c - 1;
    
    Nx = Nx*jacobian(xi,x);
    Ny = Ny*jacobian(eta,y);
    Nz = Nz*jacobian(zeta,z);

    Bi = [Nx 0 0; 0 Ny 0; 0 0 Nz; Ny Nx 0; Nz 0 Nx; 0 Nz Ny];
    B(:, 3*n-2:3*n) = Bi;
    
    clear xi eta zeta
    
end

% Young's modulus multiplied outside
D = [1 nu/(1-nu) nu/(1-nu)    0                 0           0; 
     0    1      nu/(1-nu)    0                 0           0; 
     0    0         1         0                 0           0;
     0    0         0      (1-2*nu)/(2*(1-nu))  0           0;
     0    0         0         0         (1-2*nu)/(2*(1-nu)) 0;
     0    0         0         0                 0     (1-2*nu)/(2*(1-nu))];
 
D = (1-nu)/((1+nu)*(1-2*nu))*D;
D = symmetrise(D);

% Stiffness matrix
K_N = B.'*D*B;

% Mass matrix
M_N = N.'*N;

end