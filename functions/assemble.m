function A = assemble(elementary, connectivity, N)
% Assemble elementary matrices to form system matrix
% Needs elementary matrix, connectivity and size of the problem
%
% A = ASSEMBLE(elementary, connectivity,N)
% 
% See also ASSEMBLEBC

A = sparse(N, N);

% Dof mapping
% As there are three dofs in each node
map = (connectivity-1)*3 + 1;
Ne = size(connectivity,1);

% For each element
for t=1:size(map,1)
   
    % Positions 
    % pos = [a a+1 ... a+(dof/node-1) b ...]
    
    pos = repmat(map,3,1);
    pos = reshape(pos,numel(pos),1);
    s = [zeros(size(map,1),1); ones(size(map,1),1); 2*ones(size(map,1),1)];
    s = repmat(s,27,1);
    
    pos = reshape(pos+s,Ne,numel(pos)/Ne); 
    % number of lines = number of elements 
    % number of columns = number of nodes x number of dof/node
    
    % Positions of elements of elementary matrix in complete matrix
    
    %[(a,a)    (a, a+1)   | (a,b)    (a, b+1);
    % (a+1, a) (a+1, a+1) | (a+1, b) (a+1, b+1);
    % ------------------------------------------
    % (b,a)    (b, a+1)   | (b,b)    (b, b+1);
    % (b+1, a) (b+1, a+1) | (b+1,b)  (b+1, b+1)]
    
    % row of elements: i = a a a a a+1 a+1 a+1 a+1 b b b b b+1 b+1 b+1 b+1
    % column: j = a a+1 b b+1 a a+1 b b+1 a a+1 b b+1 a a+1 b b+1
    
    % If a row of pos is taken and repeated lenght(elementary) times:
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % a a+1 a+2 ... a+gdl/dof-1 b b+1 ... b+gdl/dof-1 c c+1 ...
    % ...
    
    % making a vector with the columns gives i
    % making a vector with the rows gives j
    
    pos1 = reshape(repmat(pos(t,:),length(elementary),1), [1 length(elementary)^2]);
    pos2 = reshape(repmat(pos(t,:),1,length(elementary)), [1 length(elementary)^2]);
    
    A = A + sparse(pos1, pos2, elementary(:), N, N);
end

end