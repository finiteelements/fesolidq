function N = interpolate(terms, nodes)
% Compute interpolation functions for 3D solid
%
% N = INTERPOLATE(terms, nodes)
%
% - terms: polynomial terms used, symbolic array
% - nodes: values of the variables at nodes [x1 x2 ...; y1 y2 ...; ...]
% 
% See also ELEMENTARY

syms x y z

% Coefficients
var = length(terms); % number of variables
coef = sym('a',[1,var]); % creates a1, a2, ...

% Displacement
u(x,y,z) = coef(1:var)*terms.';

X = nodes(1,:);
Y = nodes(2,:);
Z = nodes(3,:);

% Sustitution in nodes
a = u(X,Y,Z);

% Initialisation
A = zeros(var,var);
v = 1:var;

for k = 1:length(v)

[c,t] = coeffs(a(k), coef);
n = ismember(coef,t); 
A(k,nonzeros(n.*v)) = c; % nonzeros(n.*v) finds the position of values of c in matrix A

end

N = terms/A;

% Local coordinates
syms xi eta zeta

N = subs(N,[x y z],[xi eta zeta]);

end