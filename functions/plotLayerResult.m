function plotLayerResult(Ne, result, x, y, z, connect, colour)
% Plot deformed plate meshed with quadratic 3D hexaedra
%
% PLOTLAYERRESULT(Ne, result, x, y, z, connect, colour)
%
% Ne: number of elements
% result: matrix whose columns are displacements in x, y and z
% x, y, z: coordinates
% connect: connectivity of the elements
% colour: colour code for layer
%
% See also PLOTLAYERS 

[X,Y,Z] = meshgrid(x,y,z);

% Coordinates of all points in the grid
x = reshape(permute(X,[2 1 3]),[numel(X), 1]);
y = reshape(permute(Y,[2 1 3]),[numel(Y), 1]);
z = reshape(permute(Z,[2 1 3]),[numel(Z), 1]);

% Add displacement value to coordinate
x = x + result(:,1);
y = y + result(:,2);
z = z + result(:,3);

coord = [x y z];

% Initialise
reorganised = zeros(size(coord));

n = get(gcf,'Number');
figure(n)

% Number of nodes in the element
nodeNum = 27;

for k = 1:Ne
    % Reorganise coord by element coordinates
    reorganised(nodeNum*(k-1)+1:nodeNum*(k-1)+nodeNum,:) = coord(connect(k,:),:);
end

cubePlotQ(reorganised, colour, 0.8)

end