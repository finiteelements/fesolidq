function plotLayers(Ne, x, y, z, connect, colour)
% Plot plate model meshed with 3D hexaedra
%
% PLOTMODEL(Ne, x, y, z, connect, colour)
%
% Ne: number of elements
% x, y, z: coordinates 
% connect: connectivity of the elements
% colour: color of model

[X,Y,Z] = meshgrid(x,y,z);

% Coordinates of all points in the grid, permute is used to change the
% second and first dimensions.
x = reshape(permute(X,[2 1 3]),[numel(X), 1]);
y = reshape(permute(Y,[2 1 3]),[numel(Y), 1]);
z = reshape(permute(Z,[2 1 3]),[numel(Z), 1]);

coord = [x y z];

% Initialise
reorganised = zeros(size(coord));

figure(1)

for k = 1:Ne
    
    % Reorganise coord by element coordinates
    reorganised(8*(k-1)+1:8*(k-1)+8,:) = coord(connect(k,:),:);

end

cubePlot(reorganised, colour, 0.6)

% TODO: Label the points taking into account that are quadratic
% figure(1)
% offset = a/8;
% label = cellfun(@num2str,num2cell(1:Na*Nb*Nc),'UniformOutput',0);
% h = text(x+offset,y+offset,z+offset, label);
% set(h,'Color','w')

end