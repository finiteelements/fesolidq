function F = assembleForce(a, b, c, values, connect, Ndof)
% Assemble force vector from computed elementary force vector. Needs
% lenght of element a b c for integration, values in {startNode,load,face}
% and size of the problem.
%
% F = ASSEMBLEFORCE(a, b, c, values, connect, Ndof)
%
% See also ASSEMBLEPOINTF

syms xi eta zeta x y z

% For C3D27 interpolation functions x^i�y^j�z^k / {i,j,k} in {0,1,2} 
terms = x.^(0:2).'*y.^(0:2);
terms = terms(:)*z.^(0:2);
terms = terms(:).';
nodes = [-1 -1 -1; 0 -1 -1; 1 -1 -1; 1 0 -1; 1 1 -1; 0 1 -1; -1 1 -1; -1 0 -1; 0 0 -1];

nodes = [nodes; [nodes(:,1:2) nodes(:,3)+1]; [nodes(:,1:2) nodes(:,3)+2]]';

Ni = interpolate(terms, nodes);

dof = 3;

% Initialise
F = sparse(Ndof, 1);
N = sym(zeros(dof, dof*length(Ni)));

for n = 1:length(Ni)
    
    % Matrix of interpolation functions
    % [Ni 0  0  Ni 0  0
    %  0  Ni 0  0  Ni 0
    %  0  0  Ni 0  0  Ni]
    
    N(:, 3*n-2:3*n) = Ni(n)*eye(dof);  
    
end
   
% Assemble

node = values{1}; % first node of element
load = values{2}; % for each element triplet [fx fy fz]
face = values{3}; % triplets [x y z] for example z=-1 is [0 0 -1]

% Dof mapping
map = (node-1)*3 + 1;
mapConnect = (connect-1)*3 + 1; % map connectivity

% For each element
for t=1:size(map,1)
    
    % Create elementary matrix
    Px = subs(load(t,:)',[x y z],[(xi+1)*a/2 (eta+1)*b/2 (zeta+1)*c/2]); % variable change
    
    % Surface force
    
    % Integration points
    % intP = 2; % reduced integration
    intP = 3; % full integration
    
    switch find(face(t,:)~=0) 
        
        case 1
         
            Nf = subs(N,xi,face(t,1));
            F_N = Nf.'*Px;
            fe = b/2*c/2*gaussArea(F_N, intP, [eta zeta]);
            % Positions if force in x plane
            [pos, ~] = find(connect(:,[1,2]) == node(t));
            
        case 2
        
            Nf = subs(N,eta,face(t,2));
            F_N = Nf.'*Px;
            fe = a/2*c/2*gaussArea(F_N, intP, [xi zeta]);           
            % Positions if force in y plane
            [pos, ~] = find(connect(:,[1,7]) == node(t));
            
        case 3
            Nf = subs(N,zeta,face(t,3));
            F_N = Nf.'*Px;
            fe = a/2*b/2*gaussArea(F_N, intP, [xi eta]);
            % Positions if force in z plane
            [pos, ~] = find(connect(:,[1,19]) == node(t));
    end
    
    % Body force
    % fe = a/2*b/2*c/2*gaussIntegrate(F_N,3);
    
    con = mapConnect(pos,:); % find nodes of the element
   
    % pos = [c(1)+(0:2) c(2)+(0:2) c(3)+(0:2) c(4)+(0:2) c(5)+(0:2) c(6)+(0:2) c(7)+(0:2) c(8)+(0:2)]; % organise submatrices
    
    s = 0:dof-1;
    s = reshape(repmat(s,1,length(Ni)), numel(s)*length(Ni),1);
    con = reshape(repmat(con,dof,1), numel(con)*dof,1);
    pos = con + s;
    
    F = F + sparse(pos, ones(size(pos,1),1), fe(:), Ndof, 1);
end

end