function B = gaussIntegrate(A,points)
% Integrate A matrix of symbolic entries in the space [-1 1;-1 1;-1 1]
% using the amount of points given. Return numeric value.
%
% B = GAUSSINTEGRATE(A,points)
%
% where:
% - A: symbolic function or matrix of functions to integrate
% - points: number of points to use

syms x xi eta zeta

% Compute roots of Legendre polynomial of degree 'points'
coef = coeffs(legendreP(points,x),'All'); 
p = roots(coef);

% Obtain weights
d = diff(legendreP(points,x));
w = 2./((1-p.^2).*subs(d,p).^2); 

B = 0;

for i = 1:length(p)
    
    for j = 1:length(p)
        
        for k = 1:length(p)
            B = B + w(i)*w(j)*w(k)*subs(A,[xi,eta,zeta],[p(i) p(j) p(k)]);
        end
    end
end

% Numerical value
B = double(B);

end