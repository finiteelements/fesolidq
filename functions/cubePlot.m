function cubePlot(coord, colour, alpha)
% Plot cubes given the coordinates of each cube in the following order:
%
%    8-------7
%   /|      /|
%  / |     / |
% 5--|----6  |
% |  4----|--3
% | /     | /
% 1-------2
%
% CUBEPLOT(coord, colour, alpha)
%
% where:
% - coord: coordinates of the cubes
% - colour: colour of the cubes
% - alpha: transparency of the cubes 

for k = 1:length(coord)/8

    X = coord(8*(k-1)+1:8*(k-1)+8,1);
    Y = coord(8*(k-1)+1:8*(k-1)+8,2);
    Z = coord(8*(k-1)+1:8*(k-1)+8,3);

    % Points in every face are ordered anticlockwise
    faces = [1 2 3 4; 5 6 7 8; 1 4 8 5; 2 3 7 6; 1 2 6 5; 4 3 7 8];


    % size(X) = [4 6]
    % - each column references a face
    % - there are four elements in every column, one for every point

    X = [X(faces(1,:)) X(faces(2,:)) X(faces(3,:)) X(faces(4,:)) X(faces(5,:)) X(faces(6,:))];
    Y = [Y(faces(1,:)) Y(faces(2,:)) Y(faces(3,:)) Y(faces(4,:)) Y(faces(5,:)) Y(faces(6,:))];
    Z = [Z(faces(1,:)) Z(faces(2,:)) Z(faces(3,:)) Z(faces(4,:)) Z(faces(5,:)) Z(faces(6,:))];

    fill3(X,Y,Z,colour,'FaceAlpha',alpha); % draw cube
    axis equal 

    hold on

end

end