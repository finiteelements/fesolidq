function cubePlotQ(coord, colour, alpha)
% Plot deformed cubes approximating each edge by splines given the
% coordinates of each cube in the following order:
%
%     25---24----23
%    / |         /|
%  26 12  27   22 14 
%  /   |       /  |
% 19---|20----21  |
% |    7----6-|---5
% 10  /      12  /
% |  8    9   | 4
% | /         |/
% 1-----2 ----3
%
% CUBEPLOTQ(coord, colour, alpha)
%
% where:
% - coord: coordinates of the cubes
% - colour: colour of the cubes
% - alpha: transparency of the cubes 
%
% WARNING: doesn't use the values in the middle of the faces and body

% Points in every face are ordered anticlockwise
faces = [1 2 3 4 5 6 7 8; 
         13 14 15 16 17 18 19 20; 
         1 8 7 12 19 20 13 9; 
         3 4 5 11 17 16 15 10;
         1 2 3 10 15 14 13 9; 
         7 6 5 11 17 18 19 12];

nodeNum = 27; 

% Function to filter too high data
filt = @(x,y) abs(x) < max(abs(y))*1.05;
 
for k = 1:length(coord)/nodeNum

    X = coord(nodeNum*(k-1)+1:nodeNum*(k-1)+nodeNum,1);
    Y = coord(nodeNum*(k-1)+1:nodeNum*(k-1)+nodeNum,2);
    Z = coord(nodeNum*(k-1)+1:nodeNum*(k-1)+nodeNum,3);
    
    % Remove nodes in the center of the faces and the body. From 27 to 20
    % nodes.

    X([9 11 13 15 17 18 27],:) = [];
    Y([9 11 13 15 17 18 27],:) = [];
    Z([9 11 13 15 17 18 27],:) = [];

    for t = 1:6
        
        x = X(faces(t,:));
        y = Y(faces(t,:));
        z = Z(faces(t,:));

        points = 15;

        x1 = x(1:3);
        y1 = y(1:3);
        z1 = z(1:3);
        xx1 = linspace(x1(1),x1(3),points);
        s1 = spline(x1,y1,xx1);
        sz1 = spline(x1,z1,xx1);
        
        % Filter too high data that appears because of bad conditioning
        cond = filt(s1,y1);
        s1 = s1(cond);
        xx1 = xx1(cond);
        sz1 = sz1(cond);

        cond = filt(sz1,z1);
        s1 = s1(cond);
        xx1 = xx1(cond);
        sz1 = sz1(cond);

        x2 = x(3:5);
        y2 = y(3:5);
        z2 = z(3:5);
        xx2 = linspace(x2(1),x2(3),points);
        s2 = spline(x2,y2,xx2); 
        sz2 = spline(x2,z2,xx2);
        
        % Filter too high data that appears because of bad conditioning
        cond = filt(s2,y2);
        s2 = s2(cond);
        xx2 = xx2(cond);
        sz2 = sz2(cond);

        cond = filt(sz2,z2);
        s2 = s2(cond);
        xx2 = xx2(cond);
        sz2 = sz2(cond);

        x3 = x(5:7);
        y3 = y(5:7);
        z3 = z(5:7);
        xx3 = linspace(x3(1),x3(3),points);
        s3 = spline(x3,y3,xx3);
        sz3 = spline(x3,z3,xx3);

        % Filter too high data that appears because of bad conditioning
        cond = filt(s3,y3);
        s3 = s3(cond);
        xx3 = xx3(cond);
        sz3 = sz3(cond);

        cond = filt(sz3,z3);
        s3 = s3(cond);
        xx3 = xx3(cond);
        sz3 = sz3(cond);
        
        x4 = x([7 8 1]);
        y4 = y([7 8 1]);
        z4 = z([7 8 1]);
        xx4 = linspace(x4(1),x4(3),points);
        s4 = spline(x4,y4,xx4); 
        sz4 = spline(x4,z4,xx4);
        
        % Filter too high data that appears because of bad conditioning
        cond = filt(s4,y4);
        s4 = s4(cond);
        xx4 = xx4(cond);
        sz4 = sz4(cond);

        cond = filt(sz4,z4);
        s4 = s4(cond);
        xx4 = xx4(cond);
        sz4 = sz4(cond);

        XX = [xx1 xx2 xx3 xx4];
        YY = [s1 s2 s3 s4];
        ZZ = [sz1 sz2 sz3 sz4];

        fill3(XX,YY,ZZ, colour,'FaceAlpha', alpha)
        grid on
        axis equal 
        xlabel('x'), ylabel('y'), zlabel('z')
        hold on
    
    end

end

end