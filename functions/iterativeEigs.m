function [phi, lambda] = iterativeEigs(K1_BC, K2_BC, M, E, modeN, tol)
% Compute eigenpairs iteratively for a dynamic system with a frequency 
% dependent stiffness matrix.
% 
% [phi, lambda] = ITERATIVEEIGS(K1_BC, K2_BC, M, E, modeN, tol)
%
% Input:
% - K1_BC, K2_BC: stiffness matrices of each layer with BCs applied and 
%   extended to system size
% - M: mass matrix of whole system
% - E: frequency dependent Young's modulus, symbolic
% - modeN: number of modes to compute
% - tol: tolerance

K = K1_BC+K2_BC;

[mode, lambda] = eigs(K,M,modeN,'smallestabs');
omega = real(sqrt(diag(lambda)));

phi = zeros(length(K), modeN);
lambda = zeros(modeN,1);

for r = 1:modeN
    
    % Initialisation
    deltaOmega = tol*10;

    while deltaOmega > tol
        
        [mode,lambdaNew] = eigs(K1_BC + K2_BC*double(E(omega(r))/E(0)), M, modeN,'smallestabs');
        deltaOmega = abs(real(sqrt(lambdaNew(r,r)))-omega(r));
        omega(r) = real(sqrt(lambdaNew(r,r)));
        
    end
    lambda(r) = lambdaNew(r,r);
    phi(:,r)  = mode(:,r); 
    
end

% As the boundary conditions delete the corresponded rows and columns of
% the extended stiffness matrices, they BCs can be applied first
% individually and then combine the matrices. The effect of the fractional
% modulus is that of multiplying the stiffness matrix of the second layer
% by the frequency dependent modulus.