function B = assembleBC(A, BC)
% Assemble values of variable in point with boundary conditions
%
% B = ASSEMBLEBC(A, BC)
%
% where:
% - A: matrix where BCs should be applyed
% - BC: [node, dof, value] triplets
%
% See also APPLYBC

% If there are no BC conditions the matrix stays the same 
if isempty(BC)
    
    B = A;
    return
    
end

% Extract info
node = BC(:,1);
dof  = BC(:,2);
value = BC(:,3);

map  = [(node-1)*3 + dof, value];

% Introduce values from biggest DOF not to change DOF value
map = sort(map,1,'ascend');

for i = 1:size(map,1)

    if map(i,1) == 1
        
        B = [map(i,2)*ones(1,size(A,2)); A];
        
    elseif map(i,1) > size(A,1)

        B = [A; map(i,2)*ones(1,size(A,2));];
        
    else
        
        B = [A(1:(map(i,1)-1),:); map(i,2)*ones(1,size(A,2)); A(map(i,1):end,:)];

    end

    A = B;
    
end

end