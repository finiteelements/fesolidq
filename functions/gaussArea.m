function B = gaussArea(A, points, variables)
% Integrate A matrix of symbolic entries in one side of the element
%
% B = GAUSSAREA(A, points,variables)
%
% where:
% - A: symbolic function or matrix of functions to integrate
% - points: number of points to use
% - variables: symbolic variables defining the integration area. 
% Example: [xi eta]

syms x xi eta zeta

% Compute roots of Legendre polynomial of degree 'points'
coef = coeffs(legendreP(points,x),'All'); 
p = roots(coef);

% Obtain weights
d = diff(legendreP(points,x));
w = 2./((1-p.^2).*subs(d,p).^2); 

B = 0;

for i = 1:length(p)
    
    for j = 1:length(p)
   
            B = B + w(i)*w(j)*subs(A,variables,[p(i) p(j)]);
    end
end

% Numerical value
B = double(B);

end