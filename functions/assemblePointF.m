function F = assemblePointF(values, N)
% Assemble force vector given the values of nodal forces
% Needs values of the loads in [node, dof, load]
% triplets and size of the problem.
%
% F = ASSEMBLEPOINTF(values, N)
%
% See also ASSEMBLEFORCE

node = values(:,1);
dof  = values(:,2);
load = values(:,3);

% Dof mapping
map = (node-1)*3 + dof;

F = sparse(map,ones(size(map,1),1),load,N,1);

end