* FE code for quadratic hexaedra

This repo holds the code to compute the eigenpairs and the dynamic
response of a plate discretised with C3D27 quadratic hexaedra elements.
