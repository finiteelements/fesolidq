function batchCLDresponse(ct2, boundary)
% Compute response of a CLD plate meshed with quadratic 3D solid hexaedra
% 
% - ct2: thickness of the viscoelastic layer
% - boundary: boundary condition
%
% Execute in cmd as for %H in (0.001,0.005,0.010) do (for %B in (2,3,4) do (matlab -batch "cd feSolidQ; batchCLDresponse(%H,%B)"))
% In old versions as for %H in (0.001,0.005,0.010) do (for %B in (2,3,4) do (matlab -r "batchCLDresponse(%H,%B)"))

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Plate size
at = 0.1;
bt = 0.1;
ct1 = 0.001; % first layer 
ct3 = ct1; % third layer

% Material and section data
E1 = 1.762e11;
nu1 = 0.3;
rho1 = 7782;
nu2 = 0.3;
rho2 = 1423;
E3 = E1;
nu3 = nu1;
rho3 = rho1;

E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

syms f

E2(f) = (E_0 + E_inf*(1i*tau*f)^alpha)/(1+(1i*tau*f)^alpha);

% FE model
% --------

% Mesh size only change in the thickness of the layer
Na = 21;
Nb = 21;

Nc1 = 3; % layer 1
Nc2 = 3; % layer 2
Nc3 = 3; % layer 3

dof = 3;
N = dof*(2*Na-1)*(2*Nb-1)*(2*(Nc1+Nc2+Nc3-2)-1); % size of the problem
N1 = dof*(2*Na-1)*(2*Nb-1)*(2*Nc1-1); % number of dof in first layer
N2 = dof*(2*Na-1)*(2*Nb-1)*(2*Nc2-1); % number of dof in second layer
N3 = dof*(2*Na-1)*(2*Nb-1)*(2*Nc3-1); % number of dof in third layer

% Element size
a = at/(Na-1);
b = bt/(Nb-1); 
c1 = ct1/(Nc1-1);
c2 = ct2/(Nc2-1);
c3 = ct3/(Nc3-1);

% Find interfaces
upperFace1 = (1:(2*Na-1)*(2*Nb-1))+(2*Na-1)*(2*Nb-1)*(2*Nc1-2);
upperFace2 = (1:(2*Na-1)*(2*Nb-1))+(2*Na-1)*(2*Nb-1)*(2*(Nc1+Nc2-1)-1);

% Create stiffness matrix for each layer

% Integration points
% intP = 2; % reduced integration
intP = 3; % full integration

[K_N1, M_N1] = elementary(a,b,c1,nu1);
ke1 = E1*a/2*b/2*c1/2*gaussIntegrate(K_N1, intP);
me1 = rho1*a/2*b/2*c1/2*gaussIntegrate(M_N1, intP);

[K_N2, M_N2] = elementary(a,b,c2,nu2);
ke2 = double(E2(0))*a/2*b/2*c2/2*gaussIntegrate(K_N2, intP);
me2 = rho2*a/2*b/2*c2/2*gaussIntegrate(M_N2, intP);

[K_N3, M_N3] = elementary(a,b,c3,nu3);
ke3 = E3*a/2*b/2*c3/2*gaussIntegrate(K_N3, intP);
me3 = rho3*a/2*b/2*c3/2*gaussIntegrate(M_N3, intP);

% WARNING: three integration points needed to avoid hourglassing

% Connectivity
% ------------
% First layer
connect1 = connect(Na, Nb, Nc1);

K1 = assemble(ke1, connect1, N1);
M1 = assemble(me1, connect1, N1);

% Second layer
connect2 = connect(Na, Nb, Nc2);

K2 = assemble(ke2, connect2, N2);
M2 = assemble(me2, connect2, N2);

% Third layer
% Second layer
connect3 = connect(Na, Nb, Nc3);

K3 = assemble(ke3, connect3, N3);
M3 = assemble(me3, connect3, N3);

% Assemble

% Find indices and values of matrix
[i1,j1,v1] = find(K1);
[im1,jm1,vm1] = find(M1);

[i2,j2,v2] = find(K2);
[im2,jm2,vm2] = find(M2);

[i3,j3,v3] = find(K3);
[im3,jm3,vm3] = find(M3);

% Create complete matrix
K1 = sparse(i1, j1, v1, N, N);
M1 = sparse(im1, jm1, vm1, N, N);

% Shift and add matrix of second layer
X = N1 - dof*numel(upperFace1);
K2 = sparse(i2+X, j2+X, v2, N, N);
M2 = sparse(im2+X, jm2+X, vm2, N, N);

% Shift and add matrix of third layer
X = N1 + N2 - dof*(numel(upperFace1) + numel(upperFace2));
K3 = sparse(i3+X, j3+X, v3, N, N);
M3 = sparse(im3+X, jm3+X, vm3, N, N);

K = K1 + K2 + K3;
M = M1 + M2 + M3;

% Boundary conditions
% -------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

% Boundaries
Nc = Nc1+Nc2+Nc3-2;

low = repmat(1:2*Na-1,1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Na-1,1);
s = reshape(s, 1, numel(s));
low = low + s;

up = low + (2*Na-1)*(2*Nb-2); 

left  = repmat(1:(2*Na-1):(2*Nb-1)*(2*Na-1),1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Nb-1,1);
s = reshape(s, 1, numel(s));
left = left + s;

right = left + 2*Na-2; 

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
      
    case 1 
    % Simply supported in all edges    
    % Only take the middle edge of the vertical faces
    
    low = reshape(low, numel(low)/(2*Nc-1), 2*Nc-1);    
    low = low(:,ceil(Nc/2))';
    
    up = reshape(up, numel(up)/(2*Nc-1), 2*Nc-1);    
    up = up(:,ceil(Nc/2))';
    
    right = reshape(right, numel(right)/(2*Nc-1), 2*Nc-1);    
    right = right(:,ceil(Nc/2))';
    
    left = reshape(left, numel(left)/(2*Nc-1), 2*Nc-1);    
    left = left(:,ceil(Nc/2))';
    
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];   
    
    case 2
    % Clamped in all edges
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clampled in one edge    
    border = low;
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
    case 5
    % Simply supported in all corners
    
    % Only take the middle point of the vertical edge
    
    lowLeft = lowLeft(ceil(Nc/2));
    lowRight = lowRight(ceil(Nc/2));
    upRight = upRight(ceil(Nc/2));
    upLeft = upLeft(ceil(Nc/2));
    
    border = [lowLeft lowRight upRight upLeft];
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
end

M_BC = applyBC(M, BC);

% Separate into two to apply frequency dependent modulus
K1_BC = applyBC(K1, BC);
K2_BC = applyBC(K2, BC);
K3_BC = applyBC(K3, BC);

% Response 
% --------

% Pressure in upper face z = 1
startNode = 1:(2*(2*Na-1)):(2*(Nb-1)*(2*Na-1)); % first node of every row
% Repeat nodes Na-1 times
startNode = repmat(startNode, Na-1, 1);
% Add 2 to each rep to identify first node of next rows
s = repmat(0:2:2*(Na-2), 1, Nb-1); 
% Add and sort
startNode = sort(startNode(:) + s(:)); % first node of every element
% Move to upper face
startNode = startNode + (2*Na-1)*(2*Nb-1)*(2*Nc-2);

load = [zeros(size(startNode)) zeros(size(startNode)) -1*ones(size(startNode))]; % pressure as [Px Py Pz]
face = [zeros(size(startNode)) zeros(size(startNode)) ones(size(startNode))]; % [x y z]  

values = {startNode,load,face};

F = assembleForce(a, b, c3, values, connect3 + (2*Na-1)*(2*Nb-1)*(2*(Nc1+Nc2-1)-2), N); % force applied on top layer
F_BC = applyBC(F,BC);

fmin = 0;
steps = 500;
fmax = 10000*2*pi; % to 10000 Hz

f = linspace(fmin, fmax, steps);

P = ones(steps,1); % static pressure if when f=0 --> P=1

x = zeros(length(M_BC),steps);

% Gradient

x0 = x(:,1); % Initialise 

% Precondition with unit matrix

I = eye(size(M_BC),'like', M_BC);

% use x0 = x_k-1

for k = 1:length(f) 

    s = 1i*f(k);

    K_BC = K1_BC + K2_BC*double(E2(f(k))/E2(0)) + K3_BC; 
    Z = M_BC*s^2 + K_BC;

    F = F_BC*P(k);

    [xk,~] = bicg(Z,F,1e-12,1e6,I,I,x0); % biconjugate gradients method 
    x(:,k) = xk;
    x0 = xk;

end

% Assemble x
x = assembleBC(x, BC);

% RMS response of all points (RMS of modulus)
x = x(3:3:end,:); % each column is the response at a certain f
rms = mean(abs(x).^2).^0.5;
% Take vertical deformation

figure
semilogy(f,rms)
title('RMS ')
xlabel('\omega [rad/s]'), ylabel('Amplitude [m]')
legend('3D model')

% Save data 
% ---------

save(['response/CLDsolid' num2str(ct2*1000) 'mmB' num2str(boundary) ],'f','rms')
savefig(gcf, ['figures/CLDsolid' num2str(ct2*1000) 'mmB' num2str(boundary)])