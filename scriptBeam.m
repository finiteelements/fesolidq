boundary = 6;

% FLD
ct2 = [0.002 0.006 0.010];

for i = 1:length(boundary)
    for j = 1:length(ct2)
        % batchFLDmodesBeam(ct2(j), boundary(i))
        batchFLDresponseBeam(ct2(j), boundary(i))
    end
end

% CLD
ct2 = [0.001 0.005 0.010];

for i = 1:length(boundary)
    for j = 1:length(ct2)
        % batchCLDmodesBeam(ct2(j), boundary(i))
        batchCLDresponseBeam(ct2(j), boundary(i))
    end
end
