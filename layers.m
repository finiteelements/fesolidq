% Solid model with layers of two materials
% ----------------------------------------
% Compute eigenpairs of a FLD plate meshed with quadratic 3D solid hexaedra

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Plate size
at = 1;
bt = 1;
ct1 = 0.05; % first layer 
ct2 = 0.05; % second layer 

% Material and section data

% TEST: same material for two layers
% E1 = 2e11;
% nu1 = 0.3;
% rho1 = 7900;
% E2 = 2e11; 
% nu2 = 0.3;
% rho2 = 7900;

% TEST: no damping, compare to Ansys 
E1 = 1.762e11;
nu1 = 0.3;
rho1 = 7782;
E2 = 3.53e8; 
nu2 = 0.3;
rho2 = 1423;

% FE model
% --------

% Layer 1
Na1 = 10;
Nb1 = 10;
Nc1 = 2;

% Layer 2
Na2 = Na1; % same size of first layer
Nb2 = Nb1;
Nc2 = 2;

dof = 3;
N = dof*(2*Na1-1)*(2*Nb1-1)*(2*(Nc1+Nc2-1)-1); % size of the problem
N1 = dof*(2*Na1-1)*(2*Nb1-1)*(2*Nc1-1); % number of dof in first layer
N2 = dof*(2*Na2-1)*(2*Nb2-1)*(2*Nc2-1); % number of dof in second layer

% Element size
a = at/(Na1-1);
b = bt/(Nb1-1); 
c1 = ct1/(Nc1-1);
c2 = ct2/(Nc2-1);

% Find interface
upperFace = (1:(2*Na1-1)*(2*Nb1-1))+(2*Na1-1)*(2*Nb1-1)*(2*Nc1-2);

% Create stiffness matrix for each layer

% Integration points
% intP = 2; % reduced integration
intP = 3; % full integration

[K_N1, M_N1] = elementary(a,b,c1,nu1);
ke1 = E1*a/2*b/2*c1/2*gaussIntegrate(K_N1, intP);
me1 = rho1*a/2*b/2*c1/2*gaussIntegrate(M_N1, intP);

[K_N2, M_N2] = elementary(a,b,c2,nu2);
ke2 = E2*a/2*b/2*c2/2*gaussIntegrate(K_N2, intP);
me2 = rho2*a/2*b/2*c2/2*gaussIntegrate(M_N2, intP);
% WARNING: three integration points needed to avoid hourglassing except for
% clamped case

% Connectivity
% ------------
% First layer
connect1 = connect(Na1, Nb1, Nc1);

K1 = assemble(ke1, connect1, N1);
M1 = assemble(me1, connect1, N1);

% Second layer
connect2 = connect(Na2, Nb2, Nc2);

K2 = assemble(ke2, connect2, N2);
M2 = assemble(me2, connect2, N2);

% Assemble

% Find indices and values of matrix
[i1,j1,v1] = find(K1);
[im1,jm1,vm1] = find(M1);

[i2,j2,v2] = find(K2);
[im2,jm2,vm2] = find(M2);

% Create complete matrix
K1 = sparse(i1, j1, v1, N, N);
M1 = sparse(im1, jm1, vm1, N, N);

% Shift and add matrix of second layer
X = dof*((2*Na1-1)*(2*Nb1-1)*(2*Nc1-1) - numel(upperFace));
K2 = sparse(i2+X, j2+X, v2, N, N);
M2 = sparse(im2+X, jm2+X, vm2, N, N);

K = K1 + K2;
M = M1 + M2;

% Plot model
% ----------

plotting = true; % TODO: take flags from code

% Take only outer nodes
connectOut1 = outerNodes(connect1, Na1, Nb1, Nc1);
connectOut2 = outerNodes(connect2, Na2, Nb2, Nc2);

if plotting
    % First layer

    % Number of elements
    Ne1 = (Na1-1)*(Nb1-1)*(Nc1-1);
    Ne2 = (Na2-1)*(Nb2-1)*(Nc2-1);

    % Coordinates of nodes
    x = 0:a:at; 
    y = 0:b:bt;
    z1 = 0:c1:ct1;
    z2 = (0:c2:ct2) + ct1;

    plotLayers(Ne1, x, y, z1, connectOut1, 'b')
    plotLayers(Ne2, x, y, z2, connectOut2, 'm')
end

% Boundary conditions
% -------------------

% Case 1: simply supported in all edges
% Case 2: clamped in all edges
% Case 3: free
% Case 4: clampled in one edge
% Case 5: simply supported in all corners

boundary = 1;

% Boundaries
Na = Na1;
Nb = Nb1;
Nc = Nc1+Nc2-1;

low = repmat(1:2*Na-1,1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Na-1,1);
s = reshape(s, 1, numel(s));
low = low + s;

up = low + (2*Na-1)*(2*Nb-2); 

left  = repmat(1:(2*Na-1):(2*Nb-1)*(2*Na-1),1,2*Nc-1);
s = repmat((0:2*Nc-2)*(2*Na-1)*(2*Nb-1),2*Nb-1,1);
s = reshape(s, 1, numel(s));
left = left + s;

right = left + 2*Na-2; 

% Corners
lowLeft  = intersect(low,left);
lowRight = intersect(low,right);
upLeft   = intersect(up,left);
upRight  = intersect(up,right);

switch boundary
    
    case 1 
    % Simply supported in all edges. 
    % In x = 0 and y = 0 all DOFs are blocked, in x = L and y = L
    % displacement in direction of the beam is allowed
    
    low = reshape(low, numel(low)/(2*Nc-1), 2*Nc-1);    
    low = low(:,ceil(Nc/2))';
    
    up = reshape(up, numel(up)/(2*Nc-1), 2*Nc-1);    
    up = up(:,ceil(Nc/2))';
    
    right = reshape(right, numel(right)/(2*Nc-1), 2*Nc-1);    
    right = right(:,ceil(Nc/2))';
    
    left = reshape(left, numel(left)/(2*Nc-1), 2*Nc-1);    
    left = left(:,ceil(Nc/2))';
    
    border0 = [low left];
    borderL = [up right];
    % border = unique(border);
    
    node0 = reshape(repmat(border0,dof,1), [length(border0)*dof 1]);
    dofs0 = reshape(repmat(1:dof,1,length(border0)), [length(border0)*dof 1]);
    value0 = zeros(length(border0)*dof,1);
    
    nodeL = reshape(repmat(borderL,2,1), [length(borderL)*2 1]);
    dofsL = reshape(repmat([1,3],1,length(borderL)), [length(borderL)*2 1]);
    valueL = zeros(length(borderL)*2,1);
    
    node = [node0; nodeL];
    dofs = [dofs0; dofsL];
    value = [value0; valueL];
    
    BC = [node dofs value]; 
    
    % WARNING: corners belong to two borders

      
    case 2
    % Clamped in all edges
    border = [low up right left];
    border = unique(border);
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
    
    case 3
    % Free
    BC = [];
    
    case 4
    % Clampled in one edge    
    border = low;
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
    case 5
    % Simply supported in all corners
    
    % Only take the middle point of the vertical edge
    
    lowLeft = lowLeft(ceil(Nc/2));
    lowRight = lowRight(ceil(Nc/2));
    upRight = upRight(ceil(Nc/2));
    upLeft = upLeft(ceil(Nc/2));
    
    border = [lowLeft lowRight upRight upLeft];
    
    node = reshape(repmat(border,dof,1), [length(border)*dof 1]);
    dofs = reshape(repmat(1:dof,1,length(border)), [length(border)*dof 1]);
    value = zeros(length(border)*dof,1);
    
    BC = [node dofs value];
        
end

K_BC = applyBC(K, BC);
M_BC = applyBC(M, BC);


% Eigenvalues 
% -----------

modeN = 10;

[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');

omega = sqrt(diag(lambda));

fprintf('Natural frequencies [rad/s]\n')
fprintf('%.2f\n',omega)

% Assemble BC
u = assembleBC(phi, BC);

% Plot mode
% ---------

modeNum = 5;

mode = u(:, modeNum);
scale = 2;
% WARNING: if amplitude is very small the spline will have problems to
% compute the curve, increase the scale to solve

result = scale*[mode(1:3:end) mode(2:3:end) mode(3:3:end)];
% Take apart displacement in x, y and z from the mode

if plotting
    % Coordinates of nodes
    x = linspace(0,at,2*Na-1); % take every coord
    y = linspace(0,bt,2*Nb-1); 
    z1 = linspace(0,ct1,2*Nc1-1);
    z2 = linspace(0,ct2,2*Nc2-1)+ct1;
    
    figure
    hold on
    plotLayerResult(Ne1, result(1:N1/dof, :), x, y, z1, connect1, 'b')
    plotLayerResult(Ne2, result(end-N2/dof+1:end, :), x, y, z2, connect2, 'm')
end

% Tests:
% - Two layers of same material, free, compare to Matlab --> passed
% - Two layers of same material, clamped, compare to Matlab --> passed
% - Two layers different material, compare to Ansys --> passed
% - Stability problems: check if correctly assembled --> passed: 3x3x3 
%   Gauss points needed for full integration
